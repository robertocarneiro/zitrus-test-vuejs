# zitrus-test-vuejs

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Build docker image
```
docker build -t robertocsf/zitrus-test-vuejs .
```

### Push docker image
```
docker push robertocsf/zitrus-test-vuejs
```