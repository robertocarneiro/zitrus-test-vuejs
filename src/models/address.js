export class Address {
    constructor(postalCode = '', street = '', number = '', complement = '', district = '', city = '', state = '') {
        this.postalCode = postalCode;
        this.street = street;
        this.number = number;
        this.complement = complement;
        this.district = district;
        this.city = city;
        this.state = state;
    }
}