import { authHeader, config } from '../helpers';
import { api } from '../helpers';

export const clientService = {
    findAll,
    findByCpf,
    save,
    edit,
    deleteByCpf
};

function findAll(page = 0, size = 10) {
    const headers = authHeader();
    return api.get(`${config.clients}?page=${page}&size=${size}`, { headers });
}

function findByCpf(cpf = " ") {
    const headers = authHeader();
    return api.get(`${config.clients}/${cpf}`, { headers });
}

function save(client = {}) {
    const headers = {
        'Content-Type': 'application/json',
        ...authHeader()
    };

    return api.post(config.clients, client, { headers });
}

function edit(cpf = " ", client = {}) {
    const headers = {
        'Content-Type': 'application/json',
        ...authHeader()
    };

    return api.put(`${config.clients}/${cpf}`, client, { headers });
}

function deleteByCpf(cpf = " ") {
    const headers = authHeader();
    return api.delete(`${config.clients}/${cpf}`, { headers });
}