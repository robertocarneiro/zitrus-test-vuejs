import { authHeader, config } from '../helpers';
import { api } from '../helpers';

export const addressService = {
    findByCep
};

function findByCep(cep = " ") {
    const headers = authHeader();
    return api.get(`${config.addresses}/${cep}/open-cep`, { headers });
}