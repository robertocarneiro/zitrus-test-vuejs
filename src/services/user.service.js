import { config, api, authHeader } from '../helpers'

export const userService = {
    login,
    logout,
    isLogged
};

function login(username, password) {
    const authdata = 'Basic ' + window.btoa(username + ':' + password);
    const headers = {
        'Authorization': authdata
    };

    return api.get(`${config.users}/authenticate`, { headers })
        .then(user => {
            // login successful if there's a user in the response
            if (user) {
                // store user details and basic auth credentials in local storage 
                // to keep user logged in between page refreshes
                user.authdata = authdata;
                localStorage.setItem('user', JSON.stringify(user));
            }

            return user;
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function isLogged() {
    return authHeader()['Authorization'] != null;
}