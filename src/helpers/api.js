import axios from 'axios'
import { config } from './config'
import { userService } from '../services';

export const api = axios.create({
    baseURL: config.apiUrl, //url base da sua api
    timeout: config.timeout //tempo máximo de espera de resposta
});

api.interceptors.response.use(response => {
    return response.data;
}, (error) => {
    let errorFinal = error;

    if (error && error.response) {
        if (error.response.status === 401 && !location.hash.includes('#/login')) {
            // auto logout if 401 response returned from api
            userService.logout();
            location.reload(true);
        }
        if (error.response.data && error.response.data.messages) {
            const message = error.response.data.messages.join('\n');
            errorFinal = new Error(message);
        } 
    } else if (!error || !error.response) {
        errorFinal = new Error('Erro ao comunicar com o servidor');
    }
    return Promise.reject(errorFinal);
});