export const config = {
    apiUrl: process.env.VUE_APP_API_URL,
    addresses: process.env.VUE_APP_ADDRESSES,
    clients: process.env.VUE_APP_CLIENTS,
    users: process.env.VUE_APP_USERS,
    timeout: process.env.VUE_APP_TIMEOUT
};