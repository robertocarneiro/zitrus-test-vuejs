import { createRouter, createWebHashHistory } from 'vue-router'
import ListClientView from '../views/ListClientView.vue'
import LoginView from '../views/LoginView.vue'

const routes = [
  {
    path: '/',
    name: 'list-client',
    component: ListClientView
  },
  { 
    path: '/login',
    name: 'login', 
    component: LoginView 
  },
  {
    path: '/save-client',
    name: 'create-client',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/CreateClientView.vue'),
  },
  {
    path: '/save-client/:id',
    name: 'edit-client',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/EditClientView.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/'
  }
]

export const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  if (authRequired && !loggedIn) {
    return next({ 
      path: '/login', 
      query: { returnUrl: to.path } 
    });
  }

  if (!authRequired && loggedIn) {
    return next({ 
      path: '/'
    });
  }

  next();
})